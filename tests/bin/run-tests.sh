#!/bin/bash
if [[ $1 == "all" || $1 == "patterns" ]]; then
    echo "###  RUN PATTERN TESTS    #####################"
    rspec --failure-exit-code 1 -f p ./tests/lib/patterns_spec.rb
    error="$?"
    if [ $error != 0 ]; then
        exit 1
    fi
fi

if [[ $1 == "all" || $1 == "filters" ]]; then
    echo "###  RUN FILTER Tests  ####################"
    if [[ $2 == "y" ]]; then
        logstash --configtest -f ./conf.d 2>&1
    fi

    rspec --failure-exit-code 1 -f p ./tests/lib/filter_spec.rb
    error="$?"
    if [ $error != 0 ]; then
        exit 1
    fi
fi
