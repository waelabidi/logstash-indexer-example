# Continuous integration on a Logstash indexer

This project is a simple example of Continuous integration with Logstash.

### How to use it

Put your filters files in the _conf.d_ folder.
Put your patterns in the _patterns_ folder.

Write some tests in _tests/samples/filters_ for filters and in
_tests/samples/patterns_ to test your patterns.

Rewrite the `.gitlab-ci.yml` file according to your needs and commit to your
repository.

If you want to test only patterns then write the following line in
`.gitlab-ci.yml`:
```
  - ./tests/bin/run-tests.sh patterns
```

If you only want filters tests :
```
  - ./tests/bin/run-tests.sh filters y
```

If you do not want to test the filter configuration, just remove the `y`
character.

### Source

Based on the following Github repository: https://github.com/gaspaio/logstash-tester
